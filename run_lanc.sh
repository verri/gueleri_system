mkdir -p output
make

for mu in 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6
do
  seq 1 30 | xargs -n 1 -P 8 ./run_lanc_individual.sh $mu
done
