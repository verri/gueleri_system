#include "system.hpp"

#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

auto read_neighbors(std::istream& is)
{
  auto nei = std::vector<std::vector<index_t>>();

  size_t from, to;

  while (is >> from >> to)
  {
    if (from >= to)
      continue;

    // fix indexes
    --from;
    --to;

    if (from >= nei.size())
      nei.resize(from + 1);
    nei[from].push_back(to);

    std::swap(from, to);
    if (from >= nei.size())
      nei.resize(from + 1);
    nei[from].push_back(to);
  }

  return nei;
}

int main(int argc, char *argv[]) try
{
  // Command line options
  auto desc = po::options_description{"Options"};
  desc.add_options()
    ("help,h", "Help screen")
    ("alpha", po::value<double>()->default_value(0.05), "Parameter α")
    ("ndim", po::value<std::size_t>()->default_value(3u), "Number of dimensions D")
    ("nepochs", po::value<std::size_t>()->default_value(60u), "Number of epochs")
    ("niter", po::value<std::size_t>()->default_value(1000u), "Number of iterations before checking stop")
    ("nedges", po::value<std::size_t>()->default_value(10u), "Number of removed edges per epoch")
    ("nepochskip", po::value<std::size_t>()->default_value(20u), "Number of epochs to skip while saving")
    ("epsilon", po::value<double>()->default_value(1e-4), "Stop criterion")
    ("evolution", po::bool_switch(), "Output evolution?");

  auto vm = po::variables_map();
  po::store(po::parse_command_line(argc, argv, desc), vm);

  if (vm.count("help")) {
    std::cerr << desc << '\n';
    return 0;
  }

  auto system = System(vm["ndim"].as<std::size_t>(), vm["alpha"].as<double>(),
                       read_neighbors(std::cin));

  std::cerr << system.nnodes() << " particles\n";
  std::cerr << system.nedges() << " edges\n";

  std::cout.precision(std::numeric_limits<double>::max_digits10);
  std::cout << std::fixed;

  const auto nedges = vm["nedges"].as<std::size_t>();
  const auto nepochskip = vm["nepochskip"].as<std::size_t>();
  const auto niter = vm["niter"].as<std::size_t>();
  const auto epsilon = vm["epsilon"].as<double>();
  const auto evolution = vm["evolution"].as<bool>();

  for (const auto epoch : range::view::closed_indices(vm["nepochs"].as<std::size_t>()))
  {
    if (epoch % 10 == 0)
      std::cerr << "epoch " << epoch << '\n';
    while (true)
    {
      for (const auto dt : range::view::closed_indices(niter))
      {
        system.iterate();
        if (evolution && epoch % nepochskip == 0 && dt % 10 == 0)
          system.print_evolution(epoch, std::cout);
      }

      const auto diff = system.quantify_change();
      // if (epoch % nepochskip == 0)
      // {
      //   std::cerr << " - ε = " << diff <<  ", "
      //             << system.nedges() << " remaining edges...\n";
      // }

      if (diff < epsilon)
        break;
    }

    system.remove_edges(nedges);
    system.update_communities();
    system.initialize();

    if (epoch % nepochskip == 0)
    {
      const auto Q = system.modularity();
      // std::cerr << " -> Q = " << Q << '\n';
      if (!evolution)
        system.print_communities(epoch, Q, std::cout);
    }
  }

  return 0;
}
catch (const po::error &ex)
{
  std::cerr << ex.what() << '\n';
  return 1;
}
