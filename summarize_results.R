library(ggplot2)
library(igraph)
library(NMI)
library(foreach)
library(compiler)
enableJIT(3)

library(doMC)
registerDoMC(8)

mu <- seq(0.1, 0.6, by = 0.05)
i <- 1:30

combinations <- expand.grid(mu = mu, i = i)

# result <- foreach(r = 1:nrow(combinations), .combine = rbind, .multicombine = TRUE) %dopar%
#   with(as.list(combinations[r, ]), {
#     cat("Processing ", r, " of ", nrow(combinations), "\n", sep = "")
#
#     edgelist <- as.matrix(read.table(sprintf("input/network,%d,%.2g.dat", i, mu)))
#     network <- simplify(graph_from_edgelist(edgelist, directed = FALSE))
#     expected <- read.table(sprintf("input/community,%d,%.2g.dat", i, mu))
#
#     predicted <- read.table(sprintf("output/community,%d,%.2g.tab", i, mu))
#     better <- which.max(predicted[, 2])
#
#     comm.cfg <- cluster_fast_greedy(network)
#     comm.louvain <- cluster_louvain(network)
#
#     data.frame(method = c("Proposed", "CFG", "Louvain"), mu = mu, i = i,
#                epoch = c(predicted[better, 1], NA, NA),
#                Q = c(predicted[better, 2],
#                      modularity(network, membership(comm.cfg)),
#                      modularity(network, membership(comm.louvain))),
#                nmi = c(NMI(expected, cbind(1:1000, as.numeric(predicted[better, 3:ncol(predicted)])))$value,
#                        NMI(expected, cbind(1:1000, membership(comm.cfg)))$value,
#                        NMI(expected, cbind(1:1000, membership(comm.louvain)))$value))
#   })
result <- read.table("result.tab", header = TRUE)

write.table(result, "result.tab", quote = FALSE, row.names = FALSE, col.names = TRUE)

safe_palette <- c('#d7191c','#fdae61','#abdda4','#2b83ba')
my_theme <- function(font.size = 8, font.family = 'Times', ...) {
    require('ggplot2')
    theme_bw() +
    theme(text = element_text(family = font.family, size = font.size),
          panel.grid.major = element_line(colour = 'light gray', linetype = 'dashed'),
          panel.grid.minor = element_blank(),
          panel.border = element_rect(colour = 'black'),
          panel.background = element_blank(),
          legend.key = element_blank(),#rect(color = 'black', fill = 'white'),
          legend.text = element_text(family = font.family, size = round(font.size * 0.8)),
          strip.background = element_rect(colour = 'black', fill = '#EEEEEE'),
          ...)
}

p <- ggplot(result, aes(x = factor(mu), y = nmi, fill = method)) +
      stat_boxplot(geom = "errorbar", size = 0.1) +
      geom_boxplot(color = "black", size = 0.1, outlier.size = 0.5) +
      labs(x = expression(mu), y = "Normalized mutual information", fill = "Method") +
      my_theme() + scale_colour_manual(values=safe_palette[4:1])

ggsave("plot.pdf", p, width=5, height=2, device=cairo_pdf)
