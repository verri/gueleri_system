mu=$1
i=$2
input=input/network,$i,$mu.dat
output=output/community,$i,$mu.tab
./run --niter 1 --nedges 1 --nepochs 4000 --nepochskip 1 --epsilon 1e-3 < $input > $output 2>> info.txt
