#pragma once

#include <jules/array/array.hpp>
#include <iosfwd>

using namespace jules;

class System
{
public:
  System(index_t, numeric, std::vector<std::vector<index_t>>);

  auto initialize() -> void;

  auto iterate() -> void;

  auto remove_edges(index_t) -> void;

  auto print_evolution(index_t, std::ostream&) const -> void;

  auto nedges() const -> index_t;

  auto nnodes() const -> index_t { return n; }

  auto quantify_change() const -> numeric;

  auto update_communities() -> void;

  auto modularity() const -> numeric;

  auto print_communities(index_t, std::ostream&) const -> void;

  auto print_communities(index_t, numeric, std::ostream&) const -> void;

private:
  index_t n, d, t = 0u;
  const std::vector<std::vector<index_t>> initial_neighbors;
  std::vector<std::vector<index_t>> neighbors;
  matrix<> velocity, new_velocity, mean_velocity, dv;
  numeric alpha;
  std::vector<index_t> comm; // starts in 1
};
