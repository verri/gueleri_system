SUFFIXES += .d

SRC=$(shell find src -name '*.cpp')
HEADERS=$(shell find include -name '*.hpp')
OBJ := $(SRC:.cpp=.o)
DEP := $(SRC:.cpp=.d)

CXXFLAGS=-std=c++1z -O3 -DNDEBUG -Wall -Wextra -pedantic -Iinclude -isystem../jules
LDFLAGS=-lboost_program_options

NODEPS = clean format tidy

all: run

ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEP)
endif

run: $(OBJ)
	@echo "Linking application ($@)..."
	@$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	@echo "Compiling ($<)..."
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

%.d: %.cpp
	@echo "Finding dependencies ($<)..."
	@$(CXX) $(CXXFLAGS) -MM -MT '$(patsubst %.cpp,%.o,$<)' $< -MF $@

format:
	@echo Formatting source...
	@clang-format -i -style=file $(SRC) $(HEADERS)

tidy:
	@echo Tidying source...
	@clang-tidy $(SRC) -fix -fix-errors -- $(CXXFLAGS)

clean:
	@echo "Cleaning..."
	@find . -iname '*.[od]' -exec rm {} \;

.PHONY: format tidy clean
