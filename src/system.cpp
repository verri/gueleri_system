#include "system.hpp"

#include <jules/base/random.hpp>
#include <iostream>
#include <unordered_set>

namespace
{
struct edge {
  index_t from;
  index_t to;
  numeric entropy;
};
}

System::System(index_t d, numeric alpha, std::vector<std::vector<index_t>> nei)
  : n{nei.size()}
  , d{d}
  , initial_neighbors(std::move(nei))
  , neighbors(initial_neighbors)
  , velocity(uninitialized, d, n)
  , new_velocity(uninitialized, d, n)
  , mean_velocity(uninitialized, d, 1)
  , dv(uninitialized, d, n)
  , alpha{alpha}
  , comm(n, 1)
{
  initialize();
}

auto System::initialize() -> void
{
  std::generate(velocity.begin(), velocity.end(),
    [dist = std::normal_distribution<numeric>(0.0, 1.0)]() mutable { return sample(dist); });
}

auto System::iterate() -> void
{
  ++t;

  // Normalize first
  for (const auto i : indices(n))
    velocity[every][i] = velocity[every][i] / sqrt(sum(square(velocity[every][i])));

  for (const auto i : indices(n))
  {
    if (neighbors[i].size() == 0) {
      new_velocity[every][i] = velocity[every][i];
      continue;
    }

    mean_velocity[every][every] = 0.0;
    for (const auto j : neighbors[i])
      mean_velocity += velocity[every][j];
    mean_velocity /= neighbors[i].size();

    new_velocity[every][i] = (1.0 - alpha) * velocity[every][i] + alpha * mean_velocity;
  }

  std::swap(velocity, new_velocity);
}

auto System::remove_edges(index_t m) -> void
{
  auto edges = std::vector<edge>();
  edges.reserve(n); // guess

  for (const auto i : indices(n)) {
    for (const auto j : neighbors[i]) {
      if (j >= i)
        break;
      edges.push_back({i, j, sum(abs(velocity[every][i] - velocity[every][j]))});
      // For a more difficult case, this is necessary.
      // edges.push_back({i, j, (neighbors[i].size() + neighbors[j].size()) * sum(abs(velocity[every][i] - velocity[every][j]))});
    }
  }

  std::sort(edges.begin(), edges.end(), [](const auto& x, const auto& y) {
    return x.entropy < y.entropy;
  });

  while (m--)
  {
    if (edges.size() == 0) {
      std::cerr << "warning: all edges have been removed\n";
      break;
    }

    const auto [from, to, entropy] = edges.back();
    edges.pop_back();
    (void) entropy;

    neighbors[from].erase(std::lower_bound(neighbors[from].begin(), neighbors[from].end(), to));
    neighbors[to].erase(std::lower_bound(neighbors[to].begin(), neighbors[to].end(), from));
  }
}

auto System::nedges() const -> index_t {
  auto total = index_t{0};
  for (const auto& nei : neighbors)
    total += nei.size();
  return total / 2;
}

auto System::print_evolution(index_t epoch, std::ostream& os) const -> void
{
  for (const auto i : indices(n)) {
    os << epoch << ' ' << t << ' ' << i << ' ';
    for (const auto j : indices(d))
      os << velocity[j][i] << ' ';
    os << '\n';
  }
}

auto System::quantify_change() const -> numeric
{
  return max(abs(velocity - new_velocity));
}

auto System::update_communities() -> void
{
  std::fill(comm.begin(), comm.end(), 0);

  auto current = index_t{0};
  for (const auto i : indices(n))
  {
    if (comm[i] != 0)
      continue;

    comm[i] = ++current;

    auto nei = std::unordered_set<index_t>(neighbors[i].begin(), neighbors[i].end());
    while (nei.size() != 0)
    {
      const auto j = *nei.begin();
      nei.erase(nei.begin());

      if (comm[j] != 0)
        continue;

      comm[j] = current;
      nei.insert(neighbors[j].begin(), neighbors[j].end());
    }
  }
}

auto System::modularity() const -> numeric
{
  auto m = numeric{0};

  const auto ncomm = *std::max_element(comm.begin(), comm.end());

  auto e = vector<>(ncomm);
  auto a = vector<>(ncomm);

  for (const auto i : indices(n))
  {
    for (const auto j : initial_neighbors[i])
    {
      m += 1.0;
      const auto ci = comm[i] - 1;
      const auto cj = comm[j] - 1;
      a[ci] += 1.0;
      a[cj] += 1.0;
      if (ci == cj)
        e[ci] += 2.0;
    }
  }

  if (m < 0)
    return 0.0;

  auto result = numeric{0};

  for (const auto i : indices(ncomm))
  {
    result += e[i] / 2.0 / m;
    result -= square(a[i] / 2.0 / m);
  }

  return result;
}

auto System::print_communities(index_t epoch, std::ostream& os) const -> void
{
  print_communities(epoch, modularity(), os);
}

auto System::print_communities(index_t epoch, numeric Q, std::ostream& os) const -> void
{
  os << epoch << ' ' << Q;
  for (const auto c : comm)
    os << ' ' << c;
  os << '\n';
}
